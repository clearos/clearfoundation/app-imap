<?php

$lang['imap_app_description'] = 'The IMAP and POP server provides a standard set of services for mail clients (Thunderbird, Mail, Evolution, Outlook/Express etc.) to connect to the server and retrieve/display email.';
$lang['imap_app_name'] = 'IMAP and POP Server';
$lang['imap_failed_logins'] = 'IMAP Mail Failed Logins';
$lang['imap_imap'] = 'IMAP';
$lang['imap_imaps'] = 'Secure IMAP';
$lang['imap_mail_service_invalid'] = 'Mail service is invalid.';
$lang['imap_pop3'] = 'POP';
$lang['imap_pop3s'] = 'Secure POP';
$lang['imap_push_email'] = 'Push E-mail';
$lang['imap_duplicate_delivery'] = 'Duplicate Delivery';
$lang['imap_duplicate_delivery_members'] = 'Duplicate Delivery Members';
$lang['imap_duplicate_delivery_help'] = 'You can disable duplicate delivery detection on specific mailboxes.  Only active mailboxes will be listed.';
