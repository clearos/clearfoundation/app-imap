<?php

/**
 * IMAP network check controller.
 *
 * @category   apps
 * @package    imap
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/imap/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

use \clearos\apps\imap\Cyrus as Cyrus;

require clearos_app_base('network') . '/controllers/network_check.php';

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * SMTP network check controller.
 *
 * @category   apps
 * @package    imap
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/imap/
 */

class Network extends Network_Check
{
    /**
     * Network check constructor.
     */

    function __construct()
    {
        $this->load->library('imap/Cyrus');

        $list = $this->cyrus->get_service_list();

        $imaps = $this->cyrus->get_service_state(Cyrus::SERVICE_IMAPS);
        $pop3s = $this->cyrus->get_service_state(Cyrus::SERVICE_POP3S);
        $imap = $this->cyrus->get_service_state(Cyrus::SERVICE_IMAP);
        $pop3 = $this->cyrus->get_service_state(Cyrus::SERVICE_POP3);

        $rules = [];

        if ($imaps)
            $rules[] = [ 'name' => 'IMAPS', 'protocol' => 'TCP', 'port' => 993 ];

        if ($pop3s)
            $rules[] = [ 'name' => 'POP3S', 'protocol' => 'TCP', 'port' => 995 ];
        
        if ($imap)
            $rules[] = [ 'name' => 'IMAP', 'protocol' => 'TCP', 'port' => 143 ];

        if ($pop3)
            $rules[] = [ 'name' => 'POP', 'protocol' => 'TCP', 'port' => 110 ];

        parent::__construct('imap', $rules);
    }
}
